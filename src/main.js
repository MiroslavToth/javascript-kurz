"use strict";

function getRandom (min, max) {
  min = Math.ceil(min);
  max = Math.floor(max);
  return Math.floor(Math.random() * (max - min + 1)) + min;
}

function calculatePercentMinus (percent,number) {
  number = number - (number/100 * percent);
  return number;
}


function calculatePercentPlus (percent,number) {
  number = number + (number/100 * percent);
  return number;
}

let option = null;
let random_number;
let life = 100;
let life_percent = life + "%";
let food = 10;
let stone = 5;
let weapon = 2;
let food_kg = food + "kg";
let stone_pieces = stone + "ks";
let weapon_pieces = weapon + "ks";


do {

  alert("Aktuální stav: \nŽivot = "+life_percent+"\n Jídlo = "+food_kg+"\n Kámen = "+stone_pieces+"\n Zbraně = "+weapon_pieces);
  option = prompt("Zadej volbu: \n 1 - Nakrmit se \n 2 - Jít pro kámen \n 3 - Vyrobit zbraň \n 4 - Jít na lov \n 5 - Nedělat nic \n 6 - Konec");

  if (option == 1) {
    food = food - 1;
    random_number = getRandom(30, 60);
    console.log(random_number)
    console.log(life)
    life = calculatePercentPlus(random_number, life);
    console.log(life)
  }

  if (option == 2) {
    random_number = getRandom(0, 5);
    stone = stone + random_number;
    life = calculatePercentMinus(3, life);
  }
  if (option == 3) {
    life = calculatePercentMinus(6, life);
    random_number = getRandom(0, 2);
    weapon = weapon + random_number;
    stone = stone - (3 * weapon);
  }

  if (option == 4) {
    life = calculatePercentMinus(6, life);
    weapon = weapon - 1;
    random_number = getRandom(0, 5);
    food = food + random_number;
  }

  if (option == 5) {
    life = calculatePercentMinus(1, life);
  }

  if (option == 6 || option === null) {
    break;
  }

  life = parseFloat(life).toFixed(2);

  life_percent = life + "%";
  food_kg = food + "kg";
  weapon_pieces = weapon + "ks";
  stone_pieces = stone + "ks";

  console.log(food_kg)
  console.log(life_percent)
  console.log(stone_pieces)
  console.log(weapon_pieces)


} while (option != 6)

confirm("Vysledky \n Život: "+life_percent+"\nJídlo: "+food_kg+"\nKámen: "+stone_pieces+"\nZbraně: "+weapon_pieces)